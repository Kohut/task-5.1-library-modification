use V_K_Kohut_library
go
CREATE TRIGGER [tr_books_authorsiud] on BooksAuthors
after insert, update, delete
as
begiN
if @@ROWCOUNT = 0 return

declare @operation char(1)
declare @ins int = (SELECT COUNT(*) FROM inserted)
declare @del int = (SELECT count (*) from deleted)

SET @operation = 
CASE
 WHEN @ins >0 AND @del = 0 THEN 'i'
 WHEN @ins >0 AND @del >0 THEN 'U'
 WHEN @ins = 0 AND @del >0 THEN 'd'
 END

 IF @operation = 'i'
 BEGIN

  UPDATE Authors
  SET Authors.book_amount = Authors.book_amount + a.cnt
  from Authors
  inner join
  (SELECT Author_id, count (*) AS cnt
  FROM inserted
  inner join Books
  on inserted.ISBN=Books.ISBN
  group by Author_id
  )
  a
  on Authors.Author_id = a.Author_id

  UPDATE Authors
  SET Authors.book_amount = Authors.issue_amount + a.cnt
  from
  Authors
  inner join
  (SELECT Author_id, COUNT (*) AS cnt
  from inserted
  inner join Books
  on inserted.ISBN = Books.ISBN
  group by Author_id
  )
  a
  on Authors.Author_id = a.Author_id
  
  UPDATE Authors
  set Authors.total_edition = Authors.total_edition + a.sme
  from Authors
  inner join 
  (
  SELECT([Author_id], sum ([edition]) AS sme
  from inserted
  inner join Books
  on inserted.ISBN = Books.ISBN
  group by Author_id
  ) a
  on Authors.Author_id = a.Author_id
  end

  if @operation = 'U'
  BEGIN
  UPDATE Authors
  SET Authors.book_amount = Authors.book_amount + i.cnt - d.cnt
  FROM Authors
  INNER JOIN
  (SELECT Author_id, COUNT(*) AS cnt
  from inserted
  inner join Books
  on inserted.ISBN = Books.ISBN
  group by Author_id
  ) d
  ON Authors.Author_id = d.Author_id


  UPDATE Authors
  SET Autors.issue_amount = Authors.issue_amount + i.cnt - d.cnt
  from
  Authors
   INNER JOIN
  (SELECT Author_id, COUNT(*) AS cnt
  from inserted
  inner join Books
  on inserted.ISBN = Books.ISBN
  group by Author_id
  ) i
  ON Authors.Author_id = i.Author_id

  inner join(SELECT Author_id, COUNT(*) AS cnt
  from deleted
  inner join Books
  on deleted.ISBN = Books.ISBN
  group by Author_id
  ) d
  ON Authors.Author_id = i.Author_id
   
 
   UPDATE Authors
  SET Autors.total_edition = Authors.total_edition + i.sme 
  from
  Authors
   INNER JOIN
  (SELECT Author_id, COUNT(*) AS cnt
  from inserted
  inner join Books
  on inserted.ISBN = Books.ISBN
  group by Author_id
  ) i
  ON Authors.Author_id = i.Author_id

  inner join(SELECT Author_id, COUNT(*) AS cnt
  from deleted
  inner join Books
  on deleted.ISBN = Books.ISBN
  group by Author_id
  ) d
  ON Authors.Author_id = i.Author_id
  END

  IF @operation = 'D'
  BEGIN

    UPDATE Authors
  SET Autors.book_amount = Authors.book_amount -a.cnt
  from
  Authors
   INNER JOIN
  (SELECT Author_id, COUNT(*) AS cnt
  from deleted
  inner join Books
  on inserted.ISBN = Books.ISBN
  group by Author_id
  ) a
  ON Authors.Author_id = i.Author_id
  update Authors
  set Authors.issue_amount= Authos.issue_amount + a.cnt
  from Authors
  inner join(SELECT Author_id, COUNT(*) AS cnt
  from deleted
  inner join Books
  on deleted.ISBN = Books.ISBN
  group by Author_id
  ) a
  ON Authors.Author_id = a.Author_id
   UPDATE Authors
  SET Autors.total_edition = Authors.total_edition -a.sme
  from Authors
   INNER JOIN
  (SELECT Author_id, sum(edition) AS sme
  from deleted
  inner join Books
  on deleted.ISBN = Books.ISBN
  group by Author_id
  ) a
  ON Author.Author_id = a.Author_id

  end
  end

  go
