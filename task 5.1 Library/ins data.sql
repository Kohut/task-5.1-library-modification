USE master
GO
CREATE DATABASE [V_K_Kohut_synonym]
go


USE V_K_Kohut_library
GO

INSERT INTO Authors ([Author_id],[Name],[URL])
VALUES 
(1, 'J.K.Rowling', 'www.Rowling.com'),(2, 'Patrick Rothfuss', 'www.Rothhfuss.com'),(3,'Brandon Samderson', ' Sanderson.com'),(4,'Sharrilyn Kenyon', 'www.Kenyon.com'),
(5, 'Cassandra Clare', 'www.Clare.com'),
(6, 'George R.R. Martin', 'www.Martin.com'),(7,'Quino', 'www.Quino.com'),



(15, ' Leigh Bardugo' , 'www.Bardugo.com'),
(20, ' Brian K. Voughan', 'www.Vughan.com'),
(10, 'Rick Reodrdan', 'www.Reoedan.com'),

(14, 'George Turakle', 'www.Trakl.com'),
(8, 'Kenneth J. Singhelton', 'www.Singhelton.com'),


(12, 'Rainer M. Rilke', 'www.Rilke.com'),

(19, 'Jim Butcher', 'Butcher.com'),
(18, 'Shane ONeil' , 'www.ONeil.com'),
(16, 'Kristin Hannah' , 'www.Hannah.com'),
(17, 'M.Shulz', 'www.Shulz.com'),
(13, 'Bill Matterson', 'Matterson.com'),
(9, 'Sarah J. Mass', 'www.Mass.com'),
(11, 'Jorge Luis Borges', 'www.Borges.com')
GO

INSERT INTO Publishers ([Publisher_id], [Name],[URL])
VALUES
(20, 'Dorian Gibson', 'www.Gibson.com'),
(19, 'Basil Paul', ' www.Paul.com'),
(18, 'Calvin Vance' ,'www.Vance.com'),
(17, 'Joan Vaughn', 'www.vaughn.com'),
(16, 'Tiger Moreno', 'www.Moreno.com'),
(15, 'Noel Jacobsson', 'wwwJackobsson.com'),
(14, 'Linus Lowrence', 'www.Lowrence.com'),
(13, 'Amaya Sowyer', 'www.Sowyer.com'),
(12, 'Hayden Rayan', 'www.Rayan.com'),
(11, 'Carter Hartman', 'www.Hartman.com'),
(10, 'Moses Rodgers', 'www.Rodgers.com'),
(9, 'Kenneth Ware', 'www.Ware.com'),
(8, 'Alexander Nixon', 'www. Nixon.com'),
(7, 'Geroge Warner', 'www.Warner.com'),
(6, 'Yael Mcfarland', 'www.Mcfarland.com'),
(5, 'Orlando Atkins', 'www.Atkins.com'),
(4, 'Armand Osborn', 'www.osborn.com'),
(3, 'Macah Dillard', 'www.dilard.com'),
(2, 'Gage Sawyer', 'www.Sawyer.com'),
(1, 'Michel Salazar', 'www.Salazar.com')
GO

INSERT INTO Books ([ISBN],Publisher_id,URL,Price)
 VALUES
 ('16610107-6122',1, 'www.Inescapable.com', '2.05'),
 ('16611130-3936',2, 'www.Futurama.com', '5.38'),
 ('16210225-2455',3, 'www.Cherry.com', '0.18'),
 ('16450211-5035', 4, 'www.Smlling.com', '7.63'),
 ('16250223-7924', 5, 'www.Cross.com', '5.37'),
 ('16640410-6178', 6, 'www.Better.com', '8.64'),
 ('16110703-7937', 7, 'www.Shadowzone.com', '6.43'),
 ('16321112-3538', 8, 'www.Tender.com', '5.55'),
 ('16920524-2093', 9, 'www.George.com', '7.71'),
 ('16710928-3239',10, 'www.Fotoprints.com', '7.78'),
 ('16551224-9391',11, 'www.Bride.com', '0.27'),
 ('16410712-7161',12, 'www.Islan.com', '0.58'),
 ('16470510-8664',13, 'www.HeimaHeima.com', '8.83'),
 ('16530727-0420',14,'www.Ramen.com', ' 1.42'),
 ('16500312-4061',15, 'www.Happiness.com', '4.30'),
 ('16500305-3831',16, 'www.Brassed.com', '5.76'),
 ('16780205-2253',17, 'www.Endgame.com', '7.38'),
 ('16270915-3064', 18,'www.Relentless.com', '4.91'),
 ('16100329-7288',19, 'www.Legend.com', '5.44'),
 ('16470602-3035',20,'www. ToughguyToughgu.com', '2.77')
 GO
 use V_K_Kohut_library
 go
INSERT INTO BooksAuthors (BooksAuthors_id, ISBN, Author_id, Seq_No)
VALUES
(1, '16500305-3831',19,1),
(2, '16610107-6122',13,1),
(3, '16710928-3239', 8,1),
(4, '16640410-6178', 6,1),
(5, '16270915-3064', 11,1),
(6, '16500312-4061', 17,1),
(7, '16530727-0420', 9,1),
(8, '16450211-5035', 10,1),
(9, '16920524-2093', 5,1),
(10, '16100329-7288', 20,1),
(11, '16470602-3035', 12,1),
(12, '16611130-3936', 16,1),
(13, '16551224-9391', 1,1),
(14, '16250223-7924', 18,1),
(15, '16321112-3538', 2,1),
(16,'16410712-7161', 15,1),
(17, '16470510-8464', 4,1),
(18, '16110703-7937', 14,1),
(19, '16780205-2253', 3,1),
(20, '16210225-2455', 7,1)
go
use master
SELECT*FROM V_K_Kohut_library.dbo.Authors
GO

SELECT*FROM Publishers
go

SELECT*FROM Books
go
SELECT*FROM BooksAuthors
GO
SELECT*FROM Authors_Log
go